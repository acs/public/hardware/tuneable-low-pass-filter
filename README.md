# 4x Tuneable Low Pass Filter

## Description / Functional overview

Uses a 100nF capacitor and 10k potentiometer to create a tuneable low pass.
For center setting of the potentiometer, the cutoff frequency is around 320Hz.

Use J1/J3/J5/J7 for input connections
Use J2/J4/J6/J8 for output connections
Use TP1/TP2/TP3/TP4 for probing of the input connection (or one side of the potentiometer)
Use JP1/JP3/JP5/JP7 to connect the filters to a common ground (shared through the mousebites)
Use JP2/JP4/JP6/JP8 to connect the potentiometer output to the capacitor. This can be used to measure the potentiomenter in isolation.

Turn RV1/RV2/RV3/RV4 to the left to increase the cutoff frequency, turn to the right to decrease it.

Mounting holes H1/H2/H3/H4 are M3 sized holes.

## Parts

- RV1/RV2/RV3/RV4: ACP 6-L 10K
- C1/C2/C3/C4: standard through-hole 100nF ceramic disc capacitor
- Everything else can be 2.54mm pin header or pin sockets, depending on the usecase.

## Schematic

![Schematic](output/schematic.png "Schematic")

## PCB

![PCB Boardview with copper fills](output/pcb_boardview_fill.png "PCB Boardview with copper fills")

![PCB Boardview](output/pcb_boardview.png "PCB Boardview")

![PCB 3D front view](output/pcb_3d_front.png "PCB 3D front view")

![PCB 3D back view](output/pcb_3d_back.png "PCB 3D back view")

## Attributions

3D Model of Potentiometer provided by [dhaillant](https://github.com/dhaillant/kicad-3dmodels)